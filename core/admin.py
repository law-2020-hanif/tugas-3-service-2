from django.contrib import admin
from .models import ToBeCompressed

# Register your models here.

class ToBeCompressedAdmin(admin.ModelAdmin):
    pass
admin.site.register(ToBeCompressed, ToBeCompressedAdmin)
