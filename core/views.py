from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, FileResponse
from django.views.decorators.csrf import csrf_exempt
import time, threading
import os, glob, requests, json
from .models import ToBeCompressed
import zipfile, io
from django.conf import settings
from background_task import background
from django.core import management
import pika

# Create your views here.
PATH_TO_UPLOADED_FILES_FOLDER = os.path.join(settings.BASE_DIR, 'uploaded-files', 'uploads/')
PATH_TO_UPLOADED_FILES_FOLDER_WITHOUT_UPLOADS = os.path.join(settings.BASE_DIR, 'uploaded-files/')
PATH_TO_ZIPPED_FILES_FOLDER = os.path.join(settings.BASE_DIR, 'zip_output/')

@csrf_exempt
def controller(request):
    cleaningUp()
    if request.method == 'POST':
        if 'file' not in request.FILES:
            return jsonStandardizedMessage(False, 'File tidak ditemukan. Silakan unggah satu file.', 400, {})
        
        result_ = compressFile(request.FILES)
        createZipFile(result_[0], result_[1], request.headers['X-ROUTING-KEY'])

        return jsonStandardizedMessage(True, 'Berhasil upload file', 201, {'routing_key': request.headers['X-ROUTING-KEY']})
    else:
        return jsonStandardizedMessage(False, 'Untuk melakukan kompresi file, gunakan method POST', 400, {})

def jsonStandardizedMessage(is_success, message, status_code, data):
    return JsonResponse({
        'success': is_success,
        'message': message,
        'status_code': status_code,
        'data': data
    })

def dictionaryStandardizedMessage(is_success, message, status_code, data):
    return {
        'success': is_success,
        'message': message,
        'status_code': status_code,
        'data': data
    }

def cleaningUp():
    ToBeCompressed.objects.all().delete()
    uploaded_files = glob.glob(PATH_TO_UPLOADED_FILES_FOLDER + '*')
    zipped_files = glob.glob(PATH_TO_ZIPPED_FILES_FOLDER + '*')

    for uploaded_file in uploaded_files:
        os.remove(uploaded_file)
    
    for zipped_file in zipped_files:
        os.remove(zipped_file)

def compressFile(request_file):
    inbody_file = request_file['file']
    create_object = ToBeCompressed.objects.create(upload=inbody_file)
    file_name = create_object.filename()
    file_name_with_zip_extension = file_name[:-4] + '.zip'
    return [file_name, file_name_with_zip_extension]

@background(schedule=1)
def createZipFile(file_name, file_name_with_zip_extension, routing_key):
    for i in range(0, 10):
        time.sleep(0.5)
        percentage = str((i+1) * 10)
        # ngasih persen ke queue
        message = dictionaryStandardizedMessage(True, 'zipping progress', 200, {'percentage': percentage})
        publish_to_RabbitMQ_host(routing_key, message)

    zipfile_object = zipfile.ZipFile(PATH_TO_ZIPPED_FILES_FOLDER + file_name_with_zip_extension, 'w', zipfile.ZIP_DEFLATED)
    zipfile_object.write(PATH_TO_UPLOADED_FILES_FOLDER + file_name, arcname=file_name)
    zipfile_object.close()


def publish_to_RabbitMQ_host(routing_key, message):
    routing_key = str(routing_key)
    credentials = pika.PlainCredentials('0806444524', '0806444524')
    parameters = pika.ConnectionParameters('152.118.148.95', 5672, '/0806444524', credentials)


    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()

    channel.exchange_declare(exchange='1606884344', exchange_type='direct')
    
    channel.queue_declare(queue=routing_key)

    channel.basic_publish(exchange='1606884344', routing_key=routing_key, body=json.dumps(message))
    

    connection.close()