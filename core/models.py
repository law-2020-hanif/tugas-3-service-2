from django.db import models

# Create your models here.
import os
# Create your models here.

class ToBeCompressed(models.Model):
    upload = models.FileField(upload_to='uploads/')

    def filename(self):
        return os.path.basename(self.upload.name)
